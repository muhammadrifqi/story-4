from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def launch(request):
    return render(request, 'launch.html')

def about(request):
    return render(request, 'about.html')

def skills(request):
    return render(request, 'skills.html')

def experiences(request):
    return render(request, 'experiences.html')

def gallery(request):
    return render(request, 'gallery.html')

def contact(request):
    return render(request, 'contact.html')
