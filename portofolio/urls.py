from django.urls import path
from . import views

app_name = 'portofolio'

urlpatterns = [
    path('', views.launch, name='launch'),
    path('about/', views.about, name='about'),
    path('skills/', views.skills, name='skills'),
    path('experiences/', views.experiences, name='experiences'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name='contact'),
]
